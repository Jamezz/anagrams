
/**
 * @author Thomas Helms
 * CS145 - Homework #6: Anagrams
 * This program is designed to take in a dictioanry from a Set and either find the possible words that can make up a phrase
 * or find possible anagram combinations that would entirely use up the word.
 *
 */
import java.util.*;
public class Anagrams 
{
	//fields
	private Set<String> internalDictionary; //The large dictionary that is passed in from the constructor.
	private Set<String> smallDictionary;	//The trimmed dictionary that matches the phrase from getWords().
		//(It is more efficient to store the list as a field so it doesn't need to be passed around superPrint repetitively.)
	
	//constructor
	public Anagrams(Set<String> dictionary)
	{
		nullCheck(dictionary);

		this.internalDictionary = dictionary;
		this.smallDictionary = null;	//Initialize smallDictionary to null until we know it has been generated.
	}
	
	/**
	 * Returns whether the object passed in has a null value.
	 * 
	 * @param test Object passed in to check for a null value.
	 */
	private void nullCheck(Object test)
	{
		if (test == null)
		{
			throw new IllegalArgumentException();
		}
	}
	/**
	 * This method is the public method that the client will use to send in a word. The word is first checked if null,
	 * and then all spaces are removed and sent to pgetWords().
	 * 
	 * @param phrase Word sent in from client code that is to be checked against the dictionary.
	 * @return The set that has been generated from pgetWords();
	 */
	public Set<String> getWords(String phrase)
	{
		nullCheck(phrase);
		
		String combinedLetters = phrase.replaceAll(" ", "");
		
		Set<String> returnListTwo = pgetWords(combinedLetters);
		
		return returnListTwo;
		
	}
	/**
	 * This method takes in the phrase (with no spaces) and generates a smaller dictionary that contains possible combinations of words.
	 * 
	 * @param phrase Word sent in from getWords that originated from the client code.
	 * @return Completed set that will now reside in the field smallDictionary. All words are possible words that could fit in
	 * the given phrase.
	 */
	private Set<String> pgetWords(String phrase)
	{
		this.smallDictionary = new TreeSet<String>();
		LetterInventory phraseInv = new LetterInventory(phrase);
		
		Iterator<String> dictionaryIterator = internalDictionary.iterator();
		while (dictionaryIterator.hasNext())
		{
			String dictionaryIteratorString = dictionaryIterator.next();
			
			if (phraseInv.contains(dictionaryIteratorString))
			{
				smallDictionary.add(dictionaryIteratorString);
			}
		}
		return smallDictionary;
		
	}

	/**
	 * This method is the recursive algorithm designed to generate permutations of anagrams that use up all the LetterInventory words
	 * for a phrase. The method will output to the console as it goes.	
	 * 
	 * @param previousWords The words generated from farther outside the recursive loop.
	 * @param startingPoint The iterator at where the method should start from. Typically starts at the beginning of the smallDictionary.
	 * @param phrase The phrase to check anagrams against.
	 * @param max The maximum amount of words left before we force a break or System.out.println(). Max is reduced by 1 each time the recursive method goes deeper.
	 * @param needsMax Check flag, whether we need to worry about the max parameter.
	 * @return
	 */
	private Stack<String> superPrint(Stack<String> previousWords, Iterator<String> startingPoint, String phrase, int max, boolean needsMax)
	{
		
		if (phrase.equals("[]")) 
		{
			if (max == 0)
			{
				System.out.println(previousWords.toString());
				//return previousWords;
				
			}
			else if (!needsMax)	//If we don't need a max value, then just print anyways.
			{
				System.out.println(previousWords.toString());
				//return previousWords;
			}
		}		
		
		LetterInventory letters = new LetterInventory(phrase);
		Iterator<String> dictionaryIterator = startingPoint;		
					
		String temp = "";	//Weird variable isn't initialized error. temp should never not exist.
		
		while (dictionaryIterator.hasNext())
		{
			temp = dictionaryIterator.next();
			if (letters.contains(temp))
			{
				letters.subtract(temp);
				
				previousWords.add(temp);
				if(superPrint(previousWords, smallDictionary.iterator(), letters.toString(), max-1,needsMax) != null)
				{													
					return previousWords;
				}
				else
				{
					letters.add(temp);
					previousWords.remove(temp);
				}
											
			}	
		}	
		return null;
	}
/**
 * This method is the client facing method that eventually leads to the print out on the console of possible permutations of an anagram.
 * 	
 * @param phrase The word to be checked for anagrams.
 * @param max The maximum amount of words an anagram can contain. Ex: max == 2: [fish, star]
 */
	public void print(String phrase, int max)
	{
		if (phrase == null || max < 0)
			throw new IllegalArgumentException();
		
		if (smallDictionary == null)	//If smallDictionary hasn't been initialized, then we need to initialize it.
		{
			getWords(phrase);
		}
		
		//Check flag for whether we should care about the max or not. We don't care if max == 0 when the method is called the first time.
		boolean needsMaximumWords;
		if (max == 0)
			needsMaximumWords = false;
		else
			needsMaximumWords = true;
		
		Iterator<String> smallDictionaryIterator = smallDictionary.iterator();
		Stack<String> mainStack = new Stack<String>();
		
		//This loop controls the first word of each anagram generation. The iterator will go through each word in smallDictionary.
		while (smallDictionaryIterator.hasNext())
		{
			smallDictionaryIterator.next();
			
			mainStack = superPrint(mainStack, smallDictionaryIterator, phrase, max, needsMaximumWords);
		}
	}
/**
 * Similar to print() above, this method sets the max default as 0.	
 * 
 * @param phrase The word to be checked for anagrams.
 */
	public void print(String phrase)
	{
		this.print(phrase,0);
	}
	
}
